use std::env;
use std::fs::File;
use std::io::BufRead;
use std::io::BufReader;

fn parse_input(line: &String) -> f32 {
    line.parse::<f32>().unwrap()
}

fn calculate_fuel(mass: f32) -> i32 {
    (mass / 3_f32).floor() as i32 - 2_i32
}

fn main() {
    let args: Vec<String> = env::args().collect();
    let filename: &String = args.get(1).expect("You must supply a file to evaluate.");

    let f = File::open(filename).expect("File not found");
    let file = BufReader::new(&f);
    println!("Processing file {:?}", filename);

    let lines: Vec<String> = file.lines().map(|l| l.unwrap()).collect();
    let naive_fuel: i32 = lines
        .iter()
        .map(|line| calculate_fuel(parse_input(line)))
        .sum();

    println!("Naive fuel mass: {0}", naive_fuel);

    let real_fuel: i32 = lines
        .iter()
        .map(|line| {
            let mut total_fuel = 0_i32;
            let mut mass = parse_input(line);
            loop {
                let fuel = calculate_fuel(mass);
                if fuel <= 0 {
                    break;
                }
                total_fuel += fuel;
                mass = fuel as f32;
            }
            total_fuel
        })
        .sum();

    println!("Real fuel mass: {0}", real_fuel);
}
