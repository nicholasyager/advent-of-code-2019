use std::env;
use std::fs::File;
use std::io::BufRead;
use std::io::BufReader;

fn parse_input(line: &String) -> Vec<i32> {
    line.split(",")
        .map(|value| value.parse::<i32>().unwrap())
        .collect::<Vec<i32>>()
}

fn simulate_intcode(input: Vec<i32>) -> Vec<i32> {
    let mut intcode = input.clone();
    for index in (0..intcode.len()).step_by(4) {
        let opcode = intcode[index];

        if opcode == 99 {
            break;
        }

        let register1 = intcode[index + 1] as usize;
        let register2 = intcode[index + 2] as usize;
        let target = intcode[index + 3] as usize;

        match opcode {
            1 => {
                intcode[target] = intcode[register2] + intcode[register1];
            }
            2 => {
                intcode[target] = intcode[register2] * intcode[register1];
            }
            _ => panic!("Unknown opcode provided!"),
        }
    }
    return intcode;
}

fn main() {
    let args: Vec<String> = env::args().collect();
    let filename: &String = args.get(1).expect("You must supply a file to evaluate.");

    let f = File::open(filename).expect("File not found");
    let file = BufReader::new(&f);
    println!("Processing file {:?}", filename);

    let lines: Vec<String> = file.lines().map(|l| l.unwrap()).collect();
    let intcode: Vec<i32> = lines
        .iter()
        .map(|line| parse_input(line))
        .flatten()
        .collect();

    // Modify initial intcode
    let mut initial_intcode = intcode.clone();
    initial_intcode[1] = 12;
    initial_intcode[2] = 2;

    let failing_intcode = simulate_intcode(initial_intcode);
    println!("Opcode value at position 0: {}", failing_intcode[0]);

    for noun in 0..99 {
        for verb in 0..99 {
            let mut test_intcode = intcode.clone();
            test_intcode[1] = noun;
            test_intcode[2] = verb;
            let output_intcode = simulate_intcode(test_intcode);
            if output_intcode[0] == 19690720 {
                println!("Solution found: {0}", 100 * noun + verb);
                return;
            }
        }
    }
}
